var eejs = require('ep_etherpad-lite/node/eejs/');
// Add topnav to pad page
exports.eejsBlock_scripts = function (hook_name, args, cb) {
    args.content = args.content + '<script src="https://www.infini.fr/spip.php?page=topmenu.js" type="text/javascript"></script>';
    return cb();
};
// Add infini chapo on home page
exports.eejsBlock_indexWrapper = function (hook_name, args, cb) {
    args.content = args.content + eejs.require('ep_infini/templates/index.ejs');
    return cb();
};
// Add infini js on home page
exports.eejsBlock_indexCustomScripts = function (hook_name, args, cb) {
    args.content = ('<script src="https://www.infini.fr/spip.php?page=topmenu.js" type="text/javascript"></script>' +
                    '<script src="static/plugins/ep_infini/static/js/index.js"></script>');
    return cb();
};
// Add infini css on home page
exports.eejsBlock_indexCustomStyles = function (hook_name, args, cb) {
    args.content = '<link href="../static/plugins/ep_infini/static/css/index.css" rel="stylesheet">';
    return cb();
};
// Set infini favicon
exports.loadSettings = function (hook_name, context, cb) {
    context.settings.favicon = "node_modules/ep_infini/static/favicon.ico";
    return cb();
};
