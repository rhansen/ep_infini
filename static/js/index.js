/* global $, randomPadName */

$(() => {
  $('#go2Name').unbind('submit');
  $('#button').unbind('click');
});

document.addEventListener('DOMContentLoaded', function(e) {
  const randomInstanceId = () => Math.floor(Math.random() * 2) + 1; // 1 or 2
  const form = document.getElementById('go2Name');
  form.onsubmit = function(e) {
    const padname = document.getElementById('padname').value.trim();
    const instanceId = randomInstanceId();
    if (padname.length > 0) {
      window.location = `https://lite${instanceId}.infini.fr/p/${encodeURIComponent(padname)}`;
    } else {
      alert('Please enter a name');
    }
    return false;
  };
  const button = document.getElementById('button');
  button.onclick = function(e) {
    const instanceId = randomInstanceId();
    window.location = `https://lite${instanceId}.infini.fr/p/${randomPadName()}`;
  };
});